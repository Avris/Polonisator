# Avris Polonisator

An extension to [Avris Localisator](https://gitlab.com/Avris/Localisator)
and [Avris Stringer](https://gitlab.com/Avris/Stringer)
introducing support for the Polish language.

## Installation

    composer require avris/polonisator

## Usage

    $builder = (new LocalisatorBuilder())
        ->registerExtension(new LocalisatorExtension('pl'))
        ->registerExtension(new StringerExtension())
        ->registerExtension(new PolonisatorExtension());
        
    $stringer = $builder->build(Stringer::class);
    $polonisator = $builder->build(Polonisator::class);

`StringerTwig` and `PolonisatorTwig` can be registered as [Twig](https://twig.symfony.com/) extensions.

### decline

Pluralisation is not so straight-forward in Polish -- one can say
"zdobyłeś 1 punkt", "2 punkty" and "5 punktów".
**decline** takes care of choosing the right form based on a provided number:

	Zdobyłeś {{ points }} punkt{{ points|decline('','y','ów') }}

It also provides a translation selector:

    points: <polishDeclination> punkt|%count% punkty|%count% punktów

used like this:

    {{ 'points'|l({count: points}) }}

### vocative

    {{ user.firstName|vocative }}
    
This declines Polish first names into a vocative, without a need for a database with all the names (tested on over a 1000 names).

### getGender

**getGender** tries to guess a person's gender based on their name (unless explicitly provided in `$person->getGender()`).

    $polonisator->getGender($person); // returns either Stringer::MASCULINE or Stringer::FEMININE

It also provides a translation selector:

    said: <gender> powiedział|powiedziała

### genderify

**genderify** automatically adjusts some 2nd person expressions to match the recipients gender (useful for mailings).

    {% filter genderify(user.gender) %}
    <p>
        Drog{_I_A} {{ user.firstName|vocative|capitalize }}!<br/>
        Chcielibyśmy Cię poinformować, że zdobył{_ES_AS} {{ user.points }}
        {{ user.points|decline('punkt','punkty','punktów') }}!<br/>
    </p>
    <p>
        Pozdrawiamy,<br/>
        xyz
    </p>
    {% endfilter %}

Might return for some user:

> Drogi Tomku!
>
> Chcielibyśmy Cię poinformować, że zdobyłeś 932 punkty!
>
> Pozdrawiamy,
>
> xyz
  
Available tags:

 | Tag           | Male      | Female    |
 | ------------- | --------- | --------- |
 | {_A}          |           | a         |
 | {_I_A}        | i         | a         |
 | {_ES_AS}      | eś        | aś        |
 | {_Y_A}        | y         | a         |
 | {_AL_ELA}     | ął        | ęła       |
 | {_CHCIALBY}   | chciałby  | chciałaby |
 | {_POWINIEN}   | powinien  | powinna   |
 | {_MOGL}       | mógł      | mogła     |
 | {_WSZEDL}     | wszedł    | weszła    |
 | {_PAN_PANI_M} | Pan       | Pani      |
 | {_PAN_PANI_C} | Panu      | Pani      |
 | {_PAN_PANI_D} | Pana      | Pani      |
 | {_PAN_PANI_B} | Pana      | Panią     |
 | {_PAN_PANI_N} | Panem     | Panią     |
 | {_PAN_PANI_W} | Panie     | Pani      |

This list can be expanded / changed by passing a different one into `Genderify`'s constructor.

### numberInWords

See in [Avris Stringer](https://gitlab.com/Avris/Stringer)'s documentation.

Provides Polish translations, like:

	375 → trzysta siedemdziesiąt pięć
	-279 → minus dwieście siedemdziesiąt dziewięć
	146 → sto czterdzieści sześć
	10000000 → dziesięć milionów
	5371.18 → pięć tysięcy trzysta siedemdziesiąt jeden i osiemnaście setnych
	5678 → pięć i sześćset siedemdziesiąt osiem tysięcznych
	0.1 → jedna dziesiąta
	0.000908 → zero komma zero zero zero dziewięć zero osiem

### timeInWords

See in [Avris Stringer](https://gitlab.com/Avris/Stringer)'s documentation.

Provides Polish translations, like:

`$stringer->timeInWords($time, TimeInWords::MODE_NICE)` (default):

	14:15:16 → kwadrans po czternastej.
	05:13:32 → trzynaście po piątej.
	19:49:20 → za jedenaście dwudziesta.
	11:53:11 → za siedem południe.

`$stringer->timeInWords($time, TimeInWords::MODE_SHORT)`:

	14:15:16 → czternasta piętnaście.
	05:13:32 → piąta trzynaście.
	19:49:20 → dziewiętnasta czterdzieści dziewięć.
	11:53:11 → jedenasta pięćdziesiąt trzy.

`$stringer->timeInWords($time, TimeInWords::MODE_LONG)`:

	14:15:16 → czternasta piętnaście i szesnaście sekund.
	05:13:32 → piąta trzynaście i trzydzieści dwie sekundy.
	19:49:20 → dziewiętnasta czterdzieści dziewięć i dwadzieścia sekund.
	11:53:11 → jedenasta pięćdziesiąt trzy i jedenaście sekund.
    
### timeDiff

See in [Avris Stringer](https://gitlab.com/Avris/Stringer)'s documentation.

Provides Polish translations, like: "pięć lat temu", "dwadzieścia pięć dni temu", "półtorej godziny temu",
"minutę temu", "teraz", "za trzy dni".

### phone

See in [Avris Stringer](https://gitlab.com/Avris/Stringer)'s documentation.

Provides formatting of `+48` numbers.

## Framework integration

### Micrus

In your `App\App:registerModules` register the Localisator, Stringer and Polonisator module:

    yield new \Avris\Localisator\LocalisatorModule;
    yield new \Avris\Stringer\StringerModule;
    yield new \Avris\Polonisator\PolonisatorModule;
    
And it should work out of the box: `Avris\Stringer\Stringer` and `Avris\Polonisator\Polonisator` available in the container
and the Twig extensions registered.
    
### Symfony

Example `servies.yaml` config:

    Avris\Localisator\LocalisatorBuilder:
        calls:
            - [registerExtension, ['@Avris\Localisator\LocalisatorExtension']]
            - [registerExtension, ['@Avris\Polonisator\PolonisatorExtension']]
            - [registerExtension, ['@Avris\Stringer\StringerExtension']]
    Avris\Localisator\LocalisatorExtension:
        arguments:
            $locale: '%locale%'
    Avris\Polonisator\PolonisatorExtension: ~
    Avris\Stringer\StringerExtension: ~

    Avris\Localisator\LocalisatorInterface:
        factory: ['@Avris\Localisator\LocalisatorBuilder', build]
        arguments: ['Avris\Localisator\LocalisatorInterface']
    Avris\Localisator\LocalisatorTwig: ~

    Avris\Stringer\Stringer:
        factory: ['@Avris\Localisator\LocalisatorBuilder', build]
        arguments: ['Avris\Stringer\Stringer']
    Avris\Stringer\StringerTwig: ~


    Avris\Polonisator\Polonisator:
        factory: ['@Avris\Localisator\LocalisatorBuilder', build]
        arguments: ['Avris\Polonisator\Polonisator']
    Avris\Polonisator\PolonisatorTwig: ~

### Copyright ###

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
