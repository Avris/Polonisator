<?php
namespace Avris\Polonisator\Service;

final class Genderify
{
    const DEFAULT_VERSIONS = [
        '{_A}' => ['', 'a'],
        '{_I_A}' => ['i', 'a'],
        '{_ES_AS}' => ['eś', 'aś'],
        '{_Y_A}' => ['y', 'a'],
        '{_AL_ELA}' => ['ął', 'ęła'],
        '{_CHCIALBY}' => ['chciałby', 'chciałaby'],
        '{_POWINIEN}' => ['powinien', 'powinna'],
        '{_MOGL}' => ['mógł', 'mogła'],
        '{_WSZEDL}' => ['wszedł', 'weszła'],
        '{_PAN_PANI_M}' => ['Pan', 'Pani'],
        '{_PAN_PANI_C}' => ['Panu', 'Pani'],
        '{_PAN_PANI_D}' => ['Pana', 'Pani'],
        '{_PAN_PANI_B}' => ['Pana', 'Panią'],
        '{_PAN_PANI_N}' => ['Panem', 'Panią'],
        '{_PAN_PANI_W}' => ['Panie', 'Pani'],
    ];

    /** @var string[] */
    private $versions;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(array $versions = self::DEFAULT_VERSIONS)
    {
        $this->versions = $versions;
    }

    public function genderify(string $text, int $gender): string
    {
        return preg_replace_callback('#{_[A-Z_]+}#', function ($matches) use ($gender) {
            return isset($this->versions[$matches[0]])
                ? $this->versions[$matches[0]][$gender]
                : $matches[0];
        }, $text);
    }
}
