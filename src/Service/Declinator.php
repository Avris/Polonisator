<?php
namespace Avris\Polonisator\Service;

final class Declinator
{
    /**
     * 1 punkt, 2 punkty, 5 punktów
     */
    public function decline(int $number, string $nominativeSingular, string $nominative, string $genitive): string
    {
        $number = abs($number);

        if ($number === 1) {
            return $nominativeSingular;
        }

        $ones = $number % 10;
        $tens = ($number / 10) % 10;

        return ($tens !== 1 && $ones >= 2 && $ones <= 4) ? $nominative : $genitive;
    }
}
