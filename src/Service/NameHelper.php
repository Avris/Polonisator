<?php
namespace Avris\Polonisator\Service;

use Avris\Bag\BagHelper;
use Avris\Polonisator\Entity\MaleVocativeRule;
use Avris\Stringer\Stringer;

final class NameHelper
{
    const VOCATIVE_MALE_IRREGULAR = [
        'Petr' => 'Peterze',
        'Lothar' => 'Lothardzie',
        'Andriy' => 'Andriju',
        'Marko' => 'Marku',
        'Rammo' => 'Rammonie',
        'Rodryg' => 'Rodrygdzie',
        'Wawrzyniec' => 'Wawrzyńcze',
        'Franz' => 'Franz',
        'Jack' => 'Jack',
        'Dagobert' => 'Dogoberdzie',
        'Konrad' => 'Konradzie',
        'Mikhail' => 'Mikhaile',
    ];

    const VOCATIVE_FEMALE_IRREGULAR = ['Carmen', 'Myriam', 'Miriam', 'Nicol'];

    const VOCATIVE_FEMALE_U = [
        'Alla', 'Ania', 'Ela', 'Gabryela', 'Kaja', 'Maja', 'Mariola',
        'Nikola', 'Sonia', 'Sonja', 'Basia', 'Ula', 'Ola', 'Pola'
    ];

    /**
     * @var MaleVocativeRule[] $rules
     */
    private $maleVocativeRules;

    /**
     * @codeCoverageIgnore
     */
    public function __construct()
    {
        $this->maleVocativeRules = [
            new MaleVocativeRule('st', 'ście', 2),
            new MaleVocativeRule('t', 'cie', 1),
            new MaleVocativeRule('d', 'zie'),
            new MaleVocativeRule('er', 'rze', function ($name) {
                return in_array($name, ['Alexander', 'Aleksander', 'Kacper', 'Sylwester']) ? 2 : 1;
            }),
            new MaleVocativeRule('r', 'ze'),
            new MaleVocativeRule('ek', 'ku', 2),
            new MaleVocativeRule(['l', 'j', 'g', 'k'], 'u'),
            new MaleVocativeRule(['sz', 'ch', 'rz'], 'u'),
            new MaleVocativeRule(['m', 'f', 'n', 's', 'b', 'p', 'w', 'z'], 'ie'),
            new MaleVocativeRule('ł', 'le', function ($name) {
                return mb_substr($name, -2, 1) == 'e' ? 2 : 1;
            }),
        ];
    }

    public function vocative($person): string
    {
        $name = is_object($person) && method_exists($person, 'getFirstName')
            ? call_user_func([$person, 'getFirstName'])
            : (string) $person;

        $name = $this->clearMultipleNames($name);

        return $this->isFeminine($name)
            ? $this->vocativeFeminine($name)
            : $this->vocativeMasculine($name);
    }

    private function clearMultipleNames($name)
    {
        return ($pos = strpos($name, ' ')) === false
            ? $name
            : mb_substr($name, 0, $pos);
    }

    private function vocativeFeminine(string $name): string
    {
        if (in_array($name, self::VOCATIVE_FEMALE_IRREGULAR, true)) {
            return $name;
        }

        $last = in_array($name, self::VOCATIVE_FEMALE_U, true) ? 'u' : 'o';

        return mb_substr($name, 0, -1) . $last;
    }

    private function vocativeMasculine(string $name): string
    {
        if (isset(self::VOCATIVE_MALE_IRREGULAR[$name])) {
            return self::VOCATIVE_MALE_IRREGULAR[$name];
        }

        foreach ($this->maleVocativeRules as $rule) {
            if ($rule->matches($name)) {
                return $rule->apply($name);
            }
        }

        return $name;
    }

    public function getGender($people): int
    {
        if (BagHelper::isArray($people)) {
            foreach ($people as $person) {
                if ($this->getGenderSingular($person) === Stringer::MASCULINE) {
                    return Stringer::MASCULINE;
                }
            }
            return Stringer::FEMININE;
        }

        return $this->getGenderSingular($people);
    }

    private function getGenderSingular($person)
    {
        if (is_object($person)) {
            if (method_exists($person, 'getGender')) {
                return call_user_func([$person, 'getGender']);
            }

            if (method_exists($person, 'getFirstName')) {
                return $this->guessGender(call_user_func([$person, 'getFirstName']));
            }
        }

        return $this->guessGender((string) $person);
    }

    private function guessGender(string $name): int
    {
        return $this->isFeminine($name) ? Stringer::FEMININE : Stringer::MASCULINE;
    }

    private function isFeminine(string $name): bool
    {
        return mb_substr($name, -1) === 'a' || in_array($name, self::VOCATIVE_FEMALE_IRREGULAR, true);
    }
}
