<?php
namespace Avris\Polonisator;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

final class PolonisatorModule implements ModuleInterface
{
    use ModuleTrait;
}
