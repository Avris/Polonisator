<?php
namespace Avris\Polonisator\Entity;

final class MaleVocativeRule
{
    private static $cache = [];

    /** @var string|string[] */
    private $ending;

    /** @var int */
    private $length;

    /** @var string */
    private $vocativeEnding;

    /** @var int|callable */
    private $cutLength;

    /**
     * @codeCoverageIgnore
     */
    public function __construct($ending, $vocativeEnding, $cutLength = 0)
    {
        $this->ending = $ending;
        $this->length = is_array($ending) ? mb_strlen($ending[0]) : mb_strlen($ending);
        $this->vocativeEnding = $vocativeEnding;
        $this->cutLength = $cutLength;
    }

    public function matches($name)
    {
        return is_array($this->ending)
            ? in_array($this->getLast($name), $this->ending)
            : $this->getLast($name) == $this->ending;
    }

    public function apply($name)
    {
        return $this->cut($name) . $this->vocativeEnding;
    }

    private function getLast(string $name): string
    {
        if (!isset(self::$cache[$name])) {
            self::$cache[$name] = [
                1 => mb_substr($name, -1),
                2 => mb_substr($name, -2),
            ];
        }

        return self::$cache[$name][$this->length];
    }

    private function cut($name)
    {
        if (!$this->cutLength) {
            return $name;
        }

        $cutLength = is_callable($this->cutLength)
            ? call_user_func($this->cutLength, $name)
            : $this->cutLength;

        return mb_substr($name, 0, -$cutLength);
    }
}
