<?php
namespace Avris\Polonisator\Entity;

use Avris\Stringer\Entity\Time;
use Avris\Stringer\Service\NumberInWords;
use Avris\Stringer\Stringer;

final class NiceTimeRule
{
    /** @var int */
    private $min;

    /** @var int */
    private $max;

    /** @var string */
    private $template;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(int $min, int $max, string $template)
    {
        $this->min = $min;
        $this->max = $max;
        $this->template = $template;
    }

    public function matches(Time $time): bool
    {
        return $time->getMinutes() >= $this->min && $time->getMinutes() <= $this->max;
    }

    public function apply(Time $time, NumberInWords $numberInWords, $hours)
    {
        return preg_replace_callback('#\{([^{]*)\}#', function ($matches) use ($time, $numberInWords, $hours) {
            $params = explode(':', $matches[1]);
            switch ($params[0]) {
                case 'hours':
                    return $this->formatHours($time, $params, $hours);
                case 'minutes':
                    return $this->formatMinutes($time, $params, $numberInWords);
                default:
                    return ''; // @codeCoverageIgnore
            }
        }, $this->template);
    }

    private function formatHours(Time $time, $params, $hoursNames)
    {
        if (isset($params[2]) && $params[2] == 'noon' && $time->getHours() === 12) {
            return 'południe';
        }

        $hours = isset($params[2]) && $params[2] == 'next' ? $time->getHoursNext() : $time->getHours();

        return $hoursNames[$params[1]][$hours];
    }

    private function formatMinutes(Time $time, $params, NumberInWords $numberInWords)
    {
        $minutes = isset($params[2]) ? abs($params[2] - $time->getMinutes()) : $time->getMinutes();

        return $minutes == 1
            ? 'minut' . $params[1]
            : $numberInWords->numberInWords($minutes, Stringer::FEMININE);
    }
}
