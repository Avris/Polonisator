<?php
namespace Avris\Polonisator;

use Avris\Polonisator\Service\Declinator;
use Avris\Polonisator\Service\Genderify;
use Avris\Polonisator\Service\NameHelper;

final class Polonisator
{
    /** @var Declinator */
    private $declinator;

    /** @var NameHelper */
    private $nameHelper;

    /** @var Genderify */
    private $genderify;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(
        Declinator $declinator,
        NameHelper $nameHelper,
        Genderify $genderify
    ) {
        $this->declinator = $declinator;
        $this->nameHelper = $nameHelper;
        $this->genderify = $genderify;
    }

    public function vocative($person): string
    {
        return $this->nameHelper->vocative($person);
    }

    public function getGender($people): int
    {
        return $this->nameHelper->getGender($people);
    }

    public function genderify(string $text, int $gender): string
    {
        return $this->genderify->genderify($text, $gender);
    }

    /**
     * 1 punkt, 2 punkty, 5 punktów
     */
    public function decline(int $number, string $nominativeSingular, string $nominative, string $genitive): string
    {
        return $this->declinator->decline($number, $nominativeSingular, $nominative, $genitive);
    }
}
