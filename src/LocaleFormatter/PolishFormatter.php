<?php
namespace Avris\Polonisator\LocaleFormatter;

use Avris\Stringer\LocaleFormatter\LocaleFormatter;

abstract class PolishFormatter implements LocaleFormatter
{
    public function getLocales(): array
    {
        return ['pl', 'pl_PL'];
    }
}
