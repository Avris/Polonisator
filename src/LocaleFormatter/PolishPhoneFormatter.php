<?php

namespace Avris\Polonisator\LocaleFormatter;

use Avris\Localisator\Locale\Locale;
use Avris\Stringer\LocaleFormatter\PhoneLocaleFormatter;

final class PolishPhoneFormatter extends PolishFormatter implements PhoneLocaleFormatter
{
    const BLOCK_THREE = [50, 51, 53, 57, 60, 66, 69, 72, 73, 78, 79, 88, 70, 80];

    public function getCode(): string
    {
        return '+48';
    }

    public function format($number): string
    {
        if (substr($number, 0, 1) === '0') {
            $number = substr($number, 1);
        }

        if (strlen($number) === 9 && in_array(substr($number, 0, 2), self::BLOCK_THREE)) {
            return substr($number, 0, 3).' '.substr($number, 3, 3) . ' ' . substr($number, 6, 3);
        }

        return substr($number, 0, 2) . ' ' . substr($number, 2);
    }
}
