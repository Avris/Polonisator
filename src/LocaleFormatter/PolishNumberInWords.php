<?php
namespace Avris\Polonisator\LocaleFormatter;

use Avris\Localisator\Locale\Locale;
use Avris\Polonisator\Service\Declinator;
use Avris\Stringer\LocaleFormatter\NumberInWordsLocaleFormatter;
use Avris\Stringer\Stringer;

final class PolishNumberInWords extends PolishFormatter implements NumberInWordsLocaleFormatter
{
    const ONES = [
        0 => 'zero',
        1 => 'jeden',
        2 => 'dwa',
        3 => 'trzy',
        4 => 'cztery',
        5 => 'pięć',
        6 => 'sześć',
        7 => 'siedem',
        8 => 'osiem',
        9 => 'dziewięć',
    ];

    const ONES_FEMININE = [
        1 => 'jedna',
        2 => 'dwie',
    ];

    const TEENS = [
        1 => 'jedenaście',
        2 => 'dwanaście',
        3 => 'trzynaście',
        4 => 'czternaście',
        5 => 'piętnaście',
        6 => 'szesnaście',
        7 => 'siedemnaście',
        8 => 'osiemnaście',
        9 => 'dziewiętnaście',
    ];

    const TENS = [
        1 => 'dziesięć',
        2 => 'dwadzieścia',
        3 => 'trzydzieści',
        4 => 'czterdzieści',
        5 => 'pięćdziesiąt',
        6 => 'sześćdzisiąt',
        7 => 'siedemdziesiąt',
        8 => 'osiemdziesiąt',
        9 => 'dziewięćdziesiąt',
    ];

    const HUNDREDS = [
        1 => 'sto',
        2 => 'dwieście',
        3 => 'trzysta',
        4 => 'czterysta',
        5 => 'pięćset',
        6 => 'sześćset',
        7 => 'siedemset',
        8 => 'osiemset',
        9 => 'dziewięćset',
    ];

    const LEVELS = [
        0 => '',
        1 => 'tysiąc',
        2 => 'milion',
        3 => 'miliard',
        4 => 'bilion',
        5 => 'biliard',
        6 => 'trylion',
        7 => 'tryliard',
        8 => 'kwadrylion',
        9 => 'kwadryliard',
    ];

    const DECIMAL_LEVELS = [
        1 => 'dziesiętn',
        2 => 'setn',
        3 => 'tysięczn',
    ];

    /** @var Declinator */
    private $declinator;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(Declinator $declinator)
    {
        $this->declinator = $declinator;
    }

    public function wordifyWhole($whole, int $gender)
    {
        if ($whole == 0) {
            return 'zero';
        }

        $out = [];
        $threes = $this->splitNumberInThrees($whole);

        for ($level = count($threes)-1; $level >= 0; $level--) {
            $part = $threes[$level];
            if ($part == 0) {
                continue;
            }
            if ($part > 1 || $level == 0) {
                $out[] = $this->numberPartInWords($part, $gender);
            }
            if ($level > 0) {
                $out[] = $this->getLevelName($level, $part);
            }
        }

        return join(' ', $out);
    }

    private function splitNumberInThrees($whole)
    {
        $whole = number_format($whole, 0, '', '');
        $result = [];

        do {
            $end = substr($whole, -3);
            $whole = substr($whole, 0, -3);
            $result[] = $end;
        } while (strlen($whole) > 0);

        return $result;
    }

    private function numberPartInWords($part, int $gender = Stringer::MASCULINE)
    {
        $out = [];
        $ones = $part % 10;
        $tens = floor($part / 10) % 10;
        $hundreds = floor($part / 100) % 10;

        if ($hundreds) {
            $out[] = self::HUNDREDS[$hundreds];
        }

        if ($tens == 1 && $ones > 0) {
            $out[] = self::TEENS[$ones];
        } elseif ($tens) {
            $out[] = self::TENS[$tens];
        }

        $out[] = $this->onesInWords($ones, $tens, $part, $gender);

        return trim(join(' ', $out));
    }

    private function onesInWords($ones, $tens, $part, $gender)
    {
        if (!$ones || $tens == 1) {
            return '';
        }

        if ($gender == Stringer::FEMININE) {
            if ($part == 1) {
                return 'jedna';
            }

            if ($ones == 2) {
                return 'dwie';
            }
        }

        return $ones > 0 ? self::ONES[$ones] : '';
    }

    private function getLevelName($level, $part)
    {
        return $level == 1
            ? $this->declinator->decline($part, 'tysiąc', 'tysiące', 'tysięcy')
            : self::LEVELS[$level] . $this->declinator->decline($part, '', 'y', 'ów');
    }

    public function wordifyDecimal($decimal, int $gender)
    {
        $decimal = strpos($decimal, 'E') !== false || strpos($decimal, 'e') !== false ? 0 : $decimal;

        if ($decimal == 0) {
            return '';
        }

        return join(' ', strlen($decimal) <= 3
            ? $this->shortDecimalInWords($decimal)
            : $this->longDecimalInWords($decimal));
    }

    private function shortDecimalInWords($decimal)
    {
        return [
            $this->numberPartInWords($decimal, Stringer::FEMININE),
            self::DECIMAL_LEVELS[strlen($decimal)] . $this->declinator->decline($decimal, 'a', 'e', 'ych')
        ];
    }

    private function longDecimalInWords($decimal)
    {
        $out = [];

        for ($i = 0; $i < strlen($decimal); $i++) {
            $out[] = self::ONES[substr($decimal, $i, 1)];
        }

        return $out;
    }
}
