<?php
namespace Avris\Polonisator\LocaleFormatter;

use Avris\Polonisator\Entity\NiceTimeRule;
use Avris\Polonisator\Service\Declinator;
use Avris\Stringer\Entity\Time;
use Avris\Stringer\LocaleFormatter\TimeInWordsLocaleFormatter;
use Avris\Stringer\Service\NumberInWords;
use Avris\Stringer\Stringer;

final class PolishTimeInWords extends PolishFormatter implements TimeInWordsLocaleFormatter
{
    const HOURS = [
        'nominative' => [
            0 => 'północ',
            1 => 'pierwsza',
            2 => 'druga',
            3 => 'trzecia',
            4 => 'czwarta',
            5 => 'piąta',
            6 => 'szósta',
            7 => 'siódma',
            8 => 'ósma',
            9 => 'dziewiąta',
            10 => 'dziesiąta',
            11 => 'jedenasta',
            12 => 'dwunasta',
            13 => 'trzynasta',
            14 => 'czternasta',
            15 => 'piętnasta',
            16 => 'szesnasta',
            17 => 'siedemnasta',
            18 => 'osiemnasta',
            19 => 'dziewiętnasta',
            20 => 'dwudziesta',
            21 => 'dwudziesta pierwsza',
            22 => 'dwudziesta druga',
            23 => 'dwudziesta trzecia',
        ],
        'genitive' => [
            0 => 'północy',
            1 => 'pierwszej',
            2 => 'drugiej',
            3 => 'trzeciej',
            4 => 'czwartej',
            5 => 'piątej',
            6 => 'szóstej',
            7 => 'siódmej',
            8 => 'ósmej',
            9 => 'dziewiątej',
            10 => 'dziesiątej',
            11 => 'jedenastej',
            12 => 'dwunastej',
            13 => 'trzynastej',
            14 => 'czternastej',
            15 => 'piętnastej',
            16 => 'szesnastej',
            17 => 'siedemnastej',
            18 => 'osiemnastej',
            19 => 'dziewiętnastej',
            20 => 'dwudziestej',
            21 => 'dwudziestej pierwszej',
            22 => 'dwudziestej drugiej',
            23 => 'dwudziestej trzeciej',
        ],
    ];

    /** @var Declinator */
    private $declinator;

    /** @var NumberInWords */
    private $numberInWords;

    /** @var NiceTimeRule[] */
    private $niceTimeRules;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(Declinator $declinator, NumberInWords $numberInWords)
    {
        $this->declinator = $declinator;
        $this->numberInWords = $numberInWords;
        $this->niceTimeRules = [
            new NiceTimeRule(0, 0, '{hours:nominative:noon}'),
            new NiceTimeRule(15, 15, 'kwadrans po {hours:genitive}'),
            new NiceTimeRule(0, 20, '{minutes:a} po {hours:genitive}'),
            new NiceTimeRule(21, 29, 'za {minutes:ę:30} wpół do {hours:genitive:next}'),
            new NiceTimeRule(30, 30, 'wpół do {hours:genitive:next}'),
            new NiceTimeRule(31, 44, '{minutes:a:30} po wpół do {hours:genitive:next}'),
            new NiceTimeRule(45, 45, 'za kwadrans {hours:nominative:next}'),
            new NiceTimeRule(46, 59, 'za {minutes:ę:60} {hours:nominative:next}'),
        ];
    }

    /**
     * za kwadrans druga
     */
    public function getNice(Time $time): string
    {
        foreach ($this->niceTimeRules as $rule) {
            if ($rule->matches($time)) {
                return $rule->apply($time, $this->numberInWords, self::HOURS);
            }
        }

        return $this->getShort($time); // @codeCoverageIgnore
    }

    /**
     * pierwsza czterdzieści pięć
     */
    public function getShort(Time $time): string
    {
        return trim(sprintf(
            '%s %s',
            self::HOURS['nominative'][$time->getHours()],
            $this->describeMinutes($time->getMinutes())
        ));
    }

    /**
     * pierwsza czterdzieści pięć i czterdzieści sekund
     */
    public function getLong(Time $time): string
    {
        return trim(sprintf(
            '%s %s i %s sekund%s',
            self::HOURS['nominative'][$time->getHours()],
            $this->describeMinutes($time->getMinutes()) ?: 'zero zero',
            $this->describeSeconds($time->getSeconds()) ?: 'zero',
            $this->declinator->decline($time->getSeconds(), 'a', 'y', '')
        ));
    }

    private function describeMinutes(int $minutes): string
    {
        if (!$minutes) {
            return '';
        }

        return $minutes === 1
            ? 'jeden' // @codeCoverageIgnore
            : $this->numberInWords->numberInWords($minutes, Stringer::FEMININE);
    }

    private function describeSeconds($seconds): string
    {
        return $seconds ? $this->numberInWords->numberInWords($seconds, Stringer::FEMININE) : '';
    }
}
