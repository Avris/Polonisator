<?php
namespace Avris\Polonisator;

use Avris\Container\ContainerBuilderExtension;
use Avris\Container\ContainerInterface;
use Avris\Polonisator\Service\Declinator;
use Avris\Polonisator\Service\Genderify;
use Avris\Polonisator\Service\NameHelper;
use Avris\Polonisator\Transformer\PolishDeclinationSelector;
use Avris\Polonisator\Transformer\PolishGenderSelector;
use Avris\Polonisator\LocaleFormatter\PolishNumberInWords;
use Avris\Polonisator\LocaleFormatter\PolishPhoneFormatter;
use Avris\Polonisator\LocaleFormatter\PolishTimeInWords;
use Avris\Stringer\Service\NumberInWords;

/**
 * @codeCoverageIgnore
 */
class PolonisatorExtension implements ContainerBuilderExtension
{
    const DIR = __DIR__ . '/../translations';

    public function extend(ContainerInterface $container)
    {
        $container->setDefinition(sha1(self::DIR), [
            'resolve' => self::DIR,
            'tags' => 'translationDir',
        ]);

        $container->setDefinition(Polonisator::class, [
            'arguments' => [
                '$declinator' => '@' . Declinator::class,
                '$nameHelper' => '@' . NameHelper::class,
                '$genderify' => '@' . Genderify::class,
            ],
            'public' => true,
        ]);

        $container->setDefinition(Declinator::class, []);
        $container->setDefinition(Genderify::class, []);
        $container->setDefinition(NameHelper::class, []);

        $container->setDefinition(PolishDeclinationSelector::class, [
            'arguments' => [
                '$declinator' => '@' . Declinator::class,
            ],
            'tags' => 'translationSelector',
        ]);

        $container->setDefinition(PolishGenderSelector::class, [
            'arguments' => [
                '$nameHelper' => '@' . NameHelper::class,
            ],
            'tags' => 'translationSelector',
        ]);

        $container->setDefinition(PolishPhoneFormatter::class, ['tags' => 'phoneLocaleFormatter']);
        $container->setDefinition(PolishNumberInWords::class, [
            'arguments' => [
                '$declinator' => '@' . Declinator::class,
            ],
            'tags' => 'numberInWordsLocaleFormatter',
        ]);
        $container->setDefinition(PolishTimeInWords::class, [
            'arguments' => [
                '$declinator' => '@' . Declinator::class,
                '$numberInWords' => '@' . NumberInWords::class,
            ],
            'tags' => 'timeInWordsLocaleFormatter']);
    }
}
