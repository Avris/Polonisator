<?php
namespace Avris\Polonisator;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * @codeCoverageIgnore
 */
final class PolonisatorTwig extends AbstractExtension
{
    /** @var Polonisator */
    private $polonisator;

    public function __construct(Polonisator $polonisator)
    {
        $this->polonisator = $polonisator;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('vocative', function ($person): string {
                return $this->polonisator->vocative($person);
            }),
            new TwigFilter(
                'decline',
                function (int $number, string $nominativeSingular, string $nominative, string $genitive): string {
                    return $this->polonisator->decline($number, $nominativeSingular, $nominative, $genitive);
                }
            ),
            new TwigFilter('genderify', function (string $text, int $gender): string {
                return $this->polonisator->genderify($text, $gender);
            }),
        ];
    }
}
