<?php
namespace Avris\Polonisator\Transformer;

use Avris\Localisator\Transformer\Selector\TranslationSelector;
use Avris\Polonisator\Service\NameHelper;

final class PolishGenderSelector implements TranslationSelector
{
    /** @var NameHelper */
    private $nameHelper;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(NameHelper $nameHelper)
    {
        $this->nameHelper = $nameHelper;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return 'polishGender';
    }

    public function select(array $replacements, array $versions): string
    {
        return $versions[$this->nameHelper->getGender($replacements['%person%'])];
    }
}
