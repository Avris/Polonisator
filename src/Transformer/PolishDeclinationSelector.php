<?php
namespace Avris\Polonisator\Transformer;

use Avris\Localisator\Transformer\Selector\TranslationSelector;
use Avris\Polonisator\Service\Declinator;

final class PolishDeclinationSelector implements TranslationSelector
{
    /** @var Declinator */
    private $declinator;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(Declinator $declinator)
    {
        $this->declinator = $declinator;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getName(): string
    {
        return 'polishDeclination';
    }

    public function select(array $replacements, array $versions): string
    {
        list($nominativeSingular, $nominative, $genitive) = $versions;

        return $this->declinator->decline($replacements['%count%'], $nominativeSingular, $nominative, $genitive);
    }
}
