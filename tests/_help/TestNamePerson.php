<?php
namespace Avris\Test;

final class TestNamePerson
{
    /** @var string */
    private $firstName;

    public function __construct(string $firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }
}
