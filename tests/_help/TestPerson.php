<?php
namespace Avris\Test;


final class TestPerson
{
    /** @var int */
    private $gender;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    public function __construct(int $gender, string $firstName, string $lastName)
    {
        $this->gender = $gender;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getGender(): int
    {
        return $this->gender;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function __toString()
    {
        return $this->firstName . ' ' . $this->lastName;
    }
}
