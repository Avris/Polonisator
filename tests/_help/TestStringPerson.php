<?php
namespace Avris\Test;

final class TestStringPerson
{
    /** @var string */
    private $firstName;

    public function __construct(string $firstName)
    {
        $this->firstName = $firstName;
    }

    public function __toString(): string
    {
        return $this->firstName;
    }
}
