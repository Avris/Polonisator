<?php
namespace Avris\Polonisator\Service;

use Avris\Stringer\Stringer;
use Avris\Polonisator\BaseTest;

/**
 * @covers \Avris\Polonisator\Polonisator
 * @covers \Avris\Polonisator\Service\Genderify
 */
class GenderifyTest extends BaseTest
{
    /**
     * @dataProvider genderifyProvider
     */
    public function testGenderify($input, $gender, $output)
    {
        $this->assertEquals($output, self::$polonisator->genderify($input, $gender));
    }

    public function genderifyProvider()
    {
        return [
            ['Drog{_I_A}!<br/>Zdobył{_ES_AS} główną nagrodę!', Stringer::MASCULINE, 'Drogi!<br/>Zdobyłeś główną nagrodę!'],
            ['Drog{_I_A}!<br/>Zdobył{_ES_AS} główną nagrodę!', Stringer::FEMININE, 'Droga!<br/>Zdobyłaś główną nagrodę!'],
        ];
    }
}
