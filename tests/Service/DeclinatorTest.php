<?php
namespace Avris\Polonisator\Service;

use Avris\Stringer\Stringer;
use Avris\Polonisator\BaseTest;

/**
 * @covers \Avris\Polonisator\Polonisator
 * @covers \Avris\Polonisator\Service\Declinator
 */
class DeclinatorTest extends BaseTest
{
    /**
     * @dataProvider pointsProvider
     */
    public function testDecline($numer, $output)
    {
        $this->assertEquals($output, self::$polonisator->decline($numer, 'punkt', 'punkty', 'punktów'));
    }

    public function pointsProvider()
    {
        yield [-1, 'punkt'];
        yield [0, 'punktów'];
        yield [1, 'punkt'];
        yield [2, 'punkty'];
        yield [3, 'punkty'];
        yield [5, 'punktów'];
        yield [6, 'punktów'];
        yield [10, 'punktów'];
        yield [11, 'punktów'];
        yield [14, 'punktów'];
        yield [18, 'punktów'];
        yield [20, 'punktów'];
        yield [21, 'punktów'];
        yield [22, 'punkty'];
        yield [25, 'punktów'];
        yield [77, 'punktów'];
        yield [100, 'punktów'];
        yield [101, 'punktów'];
        yield [102, 'punkty'];
    }
}
