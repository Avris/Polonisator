<?php
namespace Avris\Polonisator\Service;

use Avris\Polonisator\BaseTest;

/**
 * @covers \Avris\Polonisator\Transformer\PolishDeclinationSelector
 * @covers \Avris\Polonisator\Transformer\PolishGenderSelector
 */
class ListifyTest extends BaseTest
{
    const COMMENTATORS = ['A','B','C','D','E','F'];
    
    /**
     * @dataProvider listifyProvider
     */
    public function testListify($commentators, $maxShown, $outputNoDesc, $outputDesc)
    {
        $this->assertEquals($outputNoDesc, self::$stringer->listify($commentators, $maxShown, 'nonexistent'));
        $this->assertEquals($outputDesc, self::$stringer->listify($commentators, $maxShown));
    }

    public function listifyProvider()
    {
        return [
            [self::COMMENTATORS, 1, '6', '6 osób skomentowało'],
            [self::COMMENTATORS, 2, 'A i 5', 'A i 5 innych osób skomentowało'],
            [self::COMMENTATORS, 3, 'A, B i 4', 'A, B i 4 inne osoby skomentowały'],
            [self::COMMENTATORS, 4, 'A, B, C i 3', 'A, B, C i 3 inne osoby skomentowały'],
            [self::COMMENTATORS, 5, 'A, B, C, D i 2', 'A, B, C, D i 2 inne osoby skomentowały'],
            [self::COMMENTATORS, 6, 'A, B, C, D, E i F', 'A, B, C, D, E i F skomentowali'],
            [self::COMMENTATORS, 7, 'A, B, C, D, E i F', 'A, B, C, D, E i F skomentowali'],
            [self::COMMENTATORS, 8, 'A, B, C, D, E i F', 'A, B, C, D, E i F skomentowali'],
            [self::COMMENTATORS, 0, 'A, B, C, D, E i F', 'A, B, C, D, E i F skomentowali'],
            [['A'], 0, 'A', 'A skomentował'],
            [['A', 'B'], 0, 'A i B', 'A i B skomentowali'],

            [['Kasia'], 0, 'Kasia', 'Kasia skomentowała'],
            [['Kasia', 'Marta'], 0, 'Kasia i Marta', 'Kasia i Marta skomentowały'],
            [['Kasia', 'Rafał'], 0, 'Kasia i Rafał', 'Kasia i Rafał skomentowali'],
            [['Rafał'], 0, 'Rafał', 'Rafał skomentował'],
            [['Grzegorz', 'Rafał'], 0, 'Grzegorz i Rafał', 'Grzegorz i Rafał skomentowali'],
        ];
    }
}
