<?php
namespace Avris\Polonisator\Service;

use Avris\Polonisator\BaseTest;

/**
 * @covers \Avris\Polonisator\LocaleFormatter\PolishFormatter
 * @covers \Avris\Polonisator\LocaleFormatter\PolishPhoneFormatter
 */
final class PolishPhoneFormatterTest extends BaseTest
{
    /**
     * @dataProvider phoneProvider
     */
    public function testPhone($input, $output, $outputWithDefault = null)
    {
        $this->assertEquals($output, self::$stringer->phone($input));
        if ($outputWithDefault) {
            $this->assertEquals($outputWithDefault, self::$stringer->phone($input, 'pl'));
        }
    }

    public function phoneProvider()
    {
        return [
            ['0048501123456', '+48 501 123 456'],
            ['+48911234567', '+48 91 1234567'],
            ['+480911234567', '+48 91 1234567'],
            ['+480202224', '+48 20 2224'],
            ['0911234567', '911234567', '+48 91 1234567'],
            ['0202224', '202224', '+48 20 2224'],
            ['001555123456', '+1 555123456'],
            ['+69012345', '+690 12345'],
        ];
    }
}
