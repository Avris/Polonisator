<?php
namespace Avris\Polonisator\LocaleFormatter;

use Avris\Stringer\Service\TimeInWords;
use Avris\Polonisator\BaseTest;

/**
 * @covers \Avris\Polonisator\LocaleFormatter\PolishFormatter
 * @covers \Avris\Polonisator\LocaleFormatter\PolishTimeInWords
 * @covers \Avris\Polonisator\Entity\NiceTimeRule
 */
class PolishTimeInWordsTest extends BaseTest
{
    /**
     * @dataProvider timesProvider
     */
    public function testTimeInWords($timeString, $modeNice, $modeShort, $modeLong)
    {
        $this->assertEquals($modeNice, self::$stringer->timeInWords($timeString, TimeInWords::MODE_NICE));
        $this->assertEquals($modeShort, self::$stringer->timeInWords($timeString, TimeInWords::MODE_SHORT));
        $this->assertEquals($modeLong, self::$stringer->timeInWords($timeString, TimeInWords::MODE_LONG));
    }

    public function timesProvider()
    {
        return [
            ['02:23:07', 'za siedem wpół do trzeciej', 'druga dwadzieścia trzy', 'druga dwadzieścia trzy i siedem sekund'],
            ['21:32:08', 'dwie po wpół do dwudziestej drugiej', 'dwudziesta pierwsza trzydzieści dwie', 'dwudziesta pierwsza trzydzieści dwie i osiem sekund'],
            ['16:31:34', 'minuta po wpół do siedemnastej', 'szesnasta trzydzieści jeden', 'szesnasta trzydzieści jeden i trzydzieści cztery sekundy'],
            ['16:44:27', 'czternaście po wpół do siedemnastej', 'szesnasta czterdzieści cztery', 'szesnasta czterdzieści cztery i dwadzieścia siedem sekund'],
            ['21:24:45', 'za sześć wpół do dwudziestej drugiej', 'dwudziesta pierwsza dwadzieścia cztery', 'dwudziesta pierwsza dwadzieścia cztery i czterdzieści pięć sekund'],
            ['12:00:45', 'południe', 'dwunasta', 'dwunasta zero zero i czterdzieści pięć sekund'],
            // TODO  ['00:00:00', 'północ', 'północ', 'północ'],

            ['21:00:00', 'dwudziesta pierwsza', 'dwudziesta pierwsza', 'dwudziesta pierwsza zero zero i zero sekund'],
            ['12:00:45', 'południe', 'dwunasta', 'dwunasta zero zero i czterdzieści pięć sekund'],
            ['12:10:00', 'dziesięć po dwunastej', 'dwunasta dziesięć', 'dwunasta dziesięć i zero sekund'],
            ['00:00:00', 'północ', 'północ', 'północ zero zero i zero sekund'],
            ['23:50:00', 'za dziesięć północ', 'dwudziesta trzecia pięćdziesiąt', 'dwudziesta trzecia pięćdziesiąt i zero sekund'],

            ['18:44:06', 'czternaście po wpół do dziewiętnastej', 'osiemnasta czterdzieści cztery', 'osiemnasta czterdzieści cztery i sześć sekund'],
            ['17:04:48', 'cztery po siedemnastej', 'siedemnasta cztery', 'siedemnasta cztery i czterdzieści osiem sekund'],
            ['03:05:25', 'pięć po trzeciej', 'trzecia pięć', 'trzecia pięć i dwadzieścia pięć sekund'],
            ['08:21:49', 'za dziewięć wpół do dziewiątej', 'ósma dwadzieścia jeden', 'ósma dwadzieścia jeden i czterdzieści dziewięć sekund'],
            ['10:02:05', 'dwie po dziesiątej', 'dziesiąta dwie', 'dziesiąta dwie i pięć sekund'],
            ['02:29:34', 'za minutę wpół do trzeciej', 'druga dwadzieścia dziewięć', 'druga dwadzieścia dziewięć i trzydzieści cztery sekundy'],
            ['02:15:34', 'kwadrans po drugiej', 'druga piętnaście', 'druga piętnaście i trzydzieści cztery sekundy'],
            ['02:30:34', 'wpół do trzeciej', 'druga trzydzieści', 'druga trzydzieści i trzydzieści cztery sekundy'],
            ['02:45:34', 'za kwadrans trzecia', 'druga czterdzieści pięć', 'druga czterdzieści pięć i trzydzieści cztery sekundy'],
            ['02:59:34', 'za minutę trzecia', 'druga pięćdziesiąt dziewięć', 'druga pięćdziesiąt dziewięć i trzydzieści cztery sekundy'],
        ];
    }

//    public function testTimeInWordsNiceNotMatched()
//    {
//        $stringer = new Stringer();
//        $r = new \ReflectionObject($stringer);
//        $rules = $r->getProperty('niceTimeRules');
//        $rules->setAccessible(true);
//        $rules->setValue($stringer, [new NiceTimeRule(0, 0, 'foo')]);
//
//        $output = $stringer->timeInWords('12:34', 'nice');
//
//        $this->assertEquals('dwunasta trzydzieści cztery', $output);
//    }
}
