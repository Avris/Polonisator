<?php
namespace Avris\Stringer\LocaleFormatter;

use Avris\Stringer\Stringer;
use Avris\Polonisator\BaseTest;

/**
 * @covers \Avris\Polonisator\LocaleFormatter\PolishFormatter
 * @covers \Avris\Polonisator\LocaleFormatter\PolishNumberInWords
 */
class PolishNumberInWordsTest extends BaseTest
{
    /**
     * @dataProvider numberInWordsProvider
     */
    public function testNumberInWords($input, $output)
    {
        $this->assertEquals($output, self::$stringer->numberInWords($input));
    }

    public function numberInWordsProvider()
    {
        return [
            [468, 'czterysta sześćdzisiąt osiem'],
            [1017, 'tysiąc siedemnaście'],
            [-37, 'minus trzydzieści siedem'],
            [324, 'trzysta dwadzieścia cztery'],
            [10000000, 'dziesięć milionów'],
            [4471.952, 'cztery tysiące czterysta siedemdziesiąt jeden i dziewięćset pięćdziesiąt dwie tysięczne'],
            [5.8181, 'pięć comma osiem jeden osiem jeden'],
            [0.000385, 'zero comma zero zero zero trzy osiem pięć'],
            ['5.2E-13', 'zero comma zero zero zero zero zero zero zero zero zero zero zero zero pięć dwa'],
        ];
    }

    /**
     * @dataProvider numberInWordsFeminineProvider
     */
    public function testnumberInWordsFeminine($input, $output)
    {
        $this->assertEquals($output, self::$stringer->numberInWords($input, Stringer::FEMININE));
    }

    public function numberInWordsFeminineProvider()
    {
        return [
            [123, 'sto dwadzieścia trzy'],
            [1, 'jedna'],
            [101, 'sto jeden'],
            [21, 'dwadzieścia jeden'],
            [22, 'dwadzieścia dwie'],
        ];
    }
}
