<?php
namespace Avris\Polonisator;

use Avris\Localisator\LocalisatorBuilder;
use Avris\Localisator\LocalisatorExtension;
use Avris\Stringer\Stringer;
use Avris\Stringer\StringerExtension;
use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase
{
    /** @var Stringer */
    protected static $stringer;

    /** @var Polonisator */
    protected static $polonisator;

    public static function setUpBeforeClass()
    {
        $builder = (new LocalisatorBuilder())
            ->registerExtension(new LocalisatorExtension('pl'))
            ->registerExtension(new StringerExtension())
            ->registerExtension(new PolonisatorExtension())
        ;

        self::$stringer = $builder->build(Stringer::class);
        self::$polonisator = $builder->build(Polonisator::class);
    }
}
